from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


class TestUtils:

    @staticmethod
    def get_configured_chrome_driver():
        service = Service(executable_path=ChromeDriverManager().install())
        driver = Chrome(service=service)
        driver.maximize_window()
        driver.implicitly_wait(10)
        return driver
