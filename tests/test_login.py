from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.login_page import LoginPage
from page_objects.main_page import MainPage
from utils.test_utils import TestUtils


class TestLogin:

    def setup(self):
        self.driver = TestUtils().get_configured_chrome_driver()
        self.login_page = LoginPage(self.driver)
        self.main_page = MainPage(self.driver)

    def teardown(self):
        self.driver.quit()

    def test_verify_that_user_cant_login_without_email(self):
        self.main_page.open()
        self.main_page.click_sign_in_register_button()
        self.login_page.click_continue_with_email_button()
        assert self.login_page.get_username_note().text == "Please enter your email address"

    def test_verify_that_user_can_login(self):
        self.main_page.open()
        self.main_page.click_sign_in_register_button()
        self.login_page.enter_email("rekruit.free@gmail.com")
        self.login_page.click_continue_with_email_button()
        self.login_page.enter_password("passworD123")
        self.login_page.click_sign_in_button()
        wait = WebDriverWait(self.driver, timeout=15)
        try:
            wait.until(ec.title_is(self.main_page.title))
        except TimeoutException:
            self.driver.refresh()
            wait.until(ec.title_is(self.main_page.title))
