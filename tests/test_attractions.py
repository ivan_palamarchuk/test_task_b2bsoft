from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from page_objects.article_page import Article
from page_objects.attractions_list_page import AttractionsListPage
from page_objects.attractions_page import AttractionsPage
from page_objects.main_page import MainPage
from utils.test_utils import TestUtils


class TestAttractions:

    def setup(self):
        self.driver = TestUtils().get_configured_chrome_driver()
        self.main_page = MainPage(self.driver)
        self.attractions_page = AttractionsPage(self.driver)
        self.attractions_list_page = AttractionsListPage(self.driver)
        self.article_page = Article(self.driver)

    def teardown(self):
        self.driver.quit()

    def test_verify_that_right_article_in_the_top(self):
        self.main_page.open()
        self.main_page.click_attractions_button()
        self.attractions_page.enter_search_value("travel")
        self.attractions_page.click_search_button()
        self.attractions_list_page.click_museums_checkbox()
        self.attractions_list_page.choose_first_article()
        wait = WebDriverWait(self.driver, timeout=15)
        try:
            wait.until(ec.title_contains(self.article_page.first_article_title))
        except TimeoutException:
            if len(self.driver.window_handles) > 1:
                self.driver.close()
                self.driver.switch_to.window("")
            else:
                self.driver.refresh()
            wait.until(ec.title_contains(self.article_page.first_article_title))
