from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from page_objects.constants import MainPageSelectors


class MainPage:

    def __init__(self, driver) -> None:
        self.url = "https://www.booking.com/en-gb/"
        self.driver = driver
        self.selectors = MainPageSelectors()
        self.title = "Booking.com | Official site | The best hotels, flights, car rentals & accommodations"

    def click_sign_in_register_button(self):
        sign_in_button: WebElement = self.driver.find_element(By.XPATH, self.selectors.SIGN_IN_REGISTER_BUTTON_XPATH)
        sign_in_button.click()

    def click_attractions_button(self):
        attractions_button: WebElement = self.driver.find_element(By.XPATH, self.selectors.ATTRACTIONS_BUTTON_XPATH)
        attractions_button.click()

    def open(self):
        self.driver.get(self.url)
