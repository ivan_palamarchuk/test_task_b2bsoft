class LoginPageSelectors:
    CONTINUE_WITH_EMAIL_BUTTON_XPATH = "//span[contains(text(),'Continue with email')]"
    EMAIL_INPUT_XPATH = "//input[@id='username']"
    PASSWORD_INPUT_XPATH = "//input[@id='password']"
    SIGN_IN_BUTTON_XPATH = "//span[contains(text(),'Sign in')]"
    USERNAME_NOTE_XPATH = "//div[@id='username-note']"


class MainPageSelectors:
    SIGN_IN_REGISTER_BUTTON_XPATH = "//span[contains(text(),'Sign in / Register')]"
    ATTRACTIONS_BUTTON_XPATH = "//span[contains(text(),'Attractions')]"


class AttractionsPageSelectors:
    SEARCH_INPUT_XPATH = "//input[@type='search' and @name='query']"
    SEARCH_BUTTON_XPATH = "//span[contains(text(),'Search')]"


class AttractionsListPageSelectors:
    MUSEUMS_CHECKBOX_XPATH = "//span[contains(text(),'Museums')]"
    FIRST_ARTICLE_XPATH = "(//a[@data-testid='card'])[1]"
