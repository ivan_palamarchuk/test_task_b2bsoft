from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from page_objects.constants import LoginPageSelectors


class LoginPage:

    def __init__(self, driver) -> None:
        self.driver = driver
        self.selectors = LoginPageSelectors()

    def enter_email(self, email):
        email_input: WebElement = self.driver.find_element(By.XPATH, self.selectors.EMAIL_INPUT_XPATH)
        email_input.send_keys(email)

    def enter_password(self, password):
        password_input: WebElement = self.driver.find_element(By.XPATH, self.selectors.PASSWORD_INPUT_XPATH)
        password_input.send_keys(password)

    def click_continue_with_email_button(self):
        continue_with_email_button: WebElement = \
            self.driver.find_element(By.XPATH, self.selectors.CONTINUE_WITH_EMAIL_BUTTON_XPATH)
        continue_with_email_button.click()

    def click_sign_in_button(self):
        sign_in_button: WebElement = self.driver.find_element(By.XPATH, self.selectors.SIGN_IN_BUTTON_XPATH)
        sign_in_button.click()

    def get_username_note(self) -> WebElement:
        return self.driver.find_element(By.XPATH, self.selectors.USERNAME_NOTE_XPATH)
