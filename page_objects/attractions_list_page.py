import time

from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from page_objects.constants import AttractionsListPageSelectors


class AttractionsListPage:

    def __init__(self, driver) -> None:
        self.driver = driver
        self.selectors = AttractionsListPageSelectors()

    def choose_first_article(self):
        first_article: WebElement = self.driver.find_element(By.XPATH, self.selectors.FIRST_ARTICLE_XPATH)
        first_article.click()

    def click_museums_checkbox(self):
        museums_checkbox: WebElement = self.driver.find_element(By.XPATH, self.selectors.MUSEUMS_CHECKBOX_XPATH)
        museums_checkbox.click()
