import time

from selenium.webdriver import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement

from page_objects.constants import AttractionsPageSelectors


class AttractionsPage:

    def __init__(self, driver) -> None:
        self.driver = driver
        self.selectors = AttractionsPageSelectors()

    def enter_search_value(self, search_value):
        password_input: WebElement = self.driver.find_element(By.XPATH, self.selectors.SEARCH_INPUT_XPATH)
        password_input.send_keys(search_value + Keys.ENTER)
        time.sleep(3)
        password_input.send_keys(Keys.ENTER)

    def click_search_button(self):
        search_button: WebElement = self.driver.find_element(By.XPATH, self.selectors.SEARCH_BUTTON_XPATH)
        search_button.click()
